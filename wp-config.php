<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp_isatouproperties' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'bv[7_{K_V|#!x8) sd$S/}|.^/0]!3[=;9(#+/;01/.cj:N8CgC]fy`,HGFcQ%30' );
define( 'SECURE_AUTH_KEY',  '8ZDKss(i2*2/hG`V,O:5EASMer >O {$.*5k 68i`v^>p%+i=<X`7qu#iLF>:,ft' );
define( 'LOGGED_IN_KEY',    '4M%B~ZH6brP0f}!`r=yr vd%P<]/qu($N2^{M/bZ6C|bYe(_LAX8xhR^&PEceeQ>' );
define( 'NONCE_KEY',        '^CCargk;=1*DLgD@*=i5aTSWIYYLJ~@~3O{`3/4JEG4Qp|d+_%H]XBRIA_g9)U7s' );
define( 'AUTH_SALT',        '`)sy3fR3vl.R!7H@`lal &ux%O-a^RDnkNUYYoq[-1>b7^Ea={MSmgo~kF8lqq;-' );
define( 'SECURE_AUTH_SALT', 'M4D1@o&475;ewWng>&Z!ud-Rc3KZV4MesiCa1mTr}-vHHhK(cYHBWj1A{wX&{<Rr' );
define( 'LOGGED_IN_SALT',   'W(r_/>;KinzlYa_,nX{nhA~)@_=ZSHh5]t;Ai^7~OX+3) ~7eQSBu^.JI-49*X?@' );
define( 'NONCE_SALT',       '81ZE^+iMy;ThNg=rkQkd%IU+@|60c{-/];D1h/QVEje%tunbGM08&y]*H0n-c2r^' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
