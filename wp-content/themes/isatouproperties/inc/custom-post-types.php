<?php
/**
 * WP Default Custom Post Types Sample
 *
 * @package WP_Default
 */

/**
 * Create fence post type
 */
function fence_post_type() {

	$labels = array(
		'name'					=> __( 'Fences', 'isatouproperties' ),
		'singular_name'			=> __( 'Fence', 'isatouproperties' ),
		'add_new'				=> __( 'New Fence', 'isatouproperties' ),
		'add_new_item'			=> __( 'Add New Fence', 'isatouproperties' ),
		'edit_item'				=> __( 'Edit Fence', 'isatouproperties' ),
		'new_item'				=> __( 'New Fence', 'isatouproperties' ),
		'view_item'				=> __( 'View Fence', 'isatouproperties' ),
		'search_items'			=> __( 'Search Fences', 'isatouproperties' ),
		'not_found'				=>  __( 'No Fences Found', 'isatouproperties' ),
		'not_found_in_trash'	=> __( 'No Fences found in Trash', 'isatouproperties' ),
	);
	$args = array(
		'labels'		=> $labels,
		'has_archive'	=> true,
		'public'		=> true,
		'hierarchical'	=> false,
		'menu_icon'		=> 'dashicons-portfolio',
		'rewrite'		=> array( 'slug' => 'fence' ),
		'supports'		=> array(
			'title',
			'editor',
			'excerpt',
			'custom-fields',
			'thumbnail',
			'page-attributes'
		),
		'taxonomies'	=> array( 'post_tag' ),
	);
	register_post_type( 'fence', $args );

}
add_action( 'init', 'fence_post_type' );

/**
 * Create fence post type taxonomy
 */
function register_fence_taxonomy() {

	$labels = array(
		'name'				=> __( 'Categories', 'isatouproperties' ),
		'singular_name'		=> __( 'Category', 'isatouproperties' ),
		'search_items'		=> __( 'Search Categories', 'isatouproperties' ),
		'all_items'			=> __( 'All Categories', 'isatouproperties' ),
		'edit_item'			=> __( 'Edit Category', 'isatouproperties' ),
		'update_item'		=> __( 'Update Category', 'isatouproperties' ),
		'add_new_item'		=> __( 'Add New Category', 'isatouproperties' ),
		'new_item_name'		=> __( 'New Category Name', 'isatouproperties' ),
		'menu_name'			=> __( 'Categories', 'isatouproperties' ),
	);

	$args = array(
		'labels'			=> $labels,
		'hierarchical'		=> true,
		'sort'				=> true,
		'args'				=> array( 'orderby' => 'term_order' ),
		'rewrite'			=> array( 'slug'    => 'fence' ),
		'show_admin_column'	=> true
	);

	register_taxonomy( 'fence_cat', array( 'fence' ), $args);

}
add_action( 'init', 'register_fence_taxonomy' );