<?php

/**
 * Displays header site banner area
 *
 * @package WordPress
 * @subpackage Isatou_Properties
 * @since 1.0.0
 */
?>
<div class="site-banner">
	<div class="site-banner__background">
		<?php if (get_theme_mod('banner_image') || get_theme_mod('banner_slider')) : ?>
			<?php if (get_theme_mod('banner_slider')) :
				$sc = get_theme_mod('banner_slider');
				echo do_shortcode($sc);
			?>
			<?php elseif (get_theme_mod('banner_image')) : ?>
				<img src="<?php echo get_theme_mod('banner_image'); ?>" alt="Banner Image">
			<?php endif; ?>
		<?php endif; ?>
	</div>
	<div class="site-banner__text col-xl-12">
		<div class="row">
			<?php if (has_custom_logo()) :?>
				<div class="site-banner__logo">
					<?php the_custom_logo();?>
				</div>
			<?php endif;?>
		</div>
	</div>
</div>